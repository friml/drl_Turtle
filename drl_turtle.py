import turtle
import canvasvg
import random
import sys
colors = ["red", "green", "blue", "orange", "purple", "pink", "yellow"]
X=0
Y=0
turd = turtle.Turtle()
turtle.bgcolor("#2d2d2d")
turd.speed(10)
turd.hideturtle()
turtle.tracer(500,0)

try:
    RATIO = int(sys.argv[2])
except:
    RATIO = 1000

with open(sys.argv[1]) as file:
    lines = [line.rstrip('\n') for line in file]
    for line in lines:
        vertices = line.split("Y")
        if line[0] == "T":
            print("Skipping", line)
            COLOR = random.choice(colors)
        elif len(vertices) == 1:
            X=int(vertices[0][1:])/RATIO
            turd.setpos(X,Y)
            turd.dot(10,COLOR)
        elif vertices[0] == "":
            Y=int(vertices[1])/RATIO
            turd.setpos(X,Y)
            turd.dot(10,COLOR)
        else:
            X=int(vertices[0][1:])/RATIO
            Y=int(vertices[1])/RATIO
            turd.setpos(X,Y)
            turd.dot(10,COLOR)



canvas = turd.getscreen().getcanvas()
canvas.configure(background="black")
canvasvg.saveall("image.svg", canvas)


#turtle.done()
