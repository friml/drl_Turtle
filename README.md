# drl_Turtle
Simple script for viewing drl files

## Usage

```
./drl_turtle.py nameof_file.drl [scale]
```

`scale` argument is optional, it defines scale of coordinates.

Output is created as svg file called `image.svg`.

## Dependencies
- canvasvg

## Example of output
![img](img/output.svg)
